package com.atef.dao;

import com.atef.entities.Compte;
import org.springframework.data.jpa.repository.JpaRepository;



public interface CompteRepository extends JpaRepository<Compte, String> {

}
